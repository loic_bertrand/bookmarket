package net.tncy.lbe.bm.service;

import net.tncy.lbe.bm.data.Bookstore;
import net.tncy.lbe.bm.data.InventoryEntry;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class BookstoreService {

    private int currentId = 0;
    private final List<Bookstore> bookstores = new ArrayList<>();

    private final BookService bookService;

    public BookstoreService(BookService bookService) {
        this.bookService = bookService;
    }

    public int createBookstore(String name) {
        int id = currentId;
        var bookstore = new Bookstore(id);
        bookstore.setName(name);
        currentId++;
        bookstores.add(bookstore);
        return id;
    }

    public void deleteBookstore(int bookstoreId) {
        bookstores.removeIf(bookstore -> bookstore.getId() == bookstoreId);
    }

    public Bookstore getBookstoreById(int bookstoreId) {
        return bookstores.stream()
                         .filter(bookstore -> bookstore.getId() == bookstoreId)
                         .findFirst()
                         .orElse(null);
    }

    public List<Bookstore> findBookByName(String name) {
        return bookstores.stream()
                         .filter(bookstore -> bookstore.getName().equals(name))
                         .collect(toList());
    }

    public void renameBookstore(int bookstoreId, String newName) {
        var bookstore = getBookstoreById(bookstoreId);
        bookstore.setName(newName);
    }

    public void addBookToBookstore(int bookstoreId, int bookId, int quantity, float price) {
        var book = bookService.getBookById(bookId);
        if (book == null) return;
        var bookstore = getBookstoreById(bookstoreId);
        if (bookstore == null) return;
        bookstore.addInventoryEntry(new InventoryEntry(book, quantity, price, price));
    }

    public void removeBookFromBookstore(int bookstoreId, int bookId, int quantity) {
        var bookstore = getBookstoreById(bookstoreId);
        if (bookstore == null) return;
        for (var entry : bookstore.getInventoryEntries()) {
            if (entry.getBook().getId() == bookId) {
                var newQuantity = Math.min(entry.getQuantity() - quantity, 0);
                entry.setQuantity(newQuantity);
            }
        }
    }

    public List<InventoryEntry> getBookstoreInventory(int bookstoreId) {
        var bookstore = getBookstoreById(bookstoreId);
        if (bookstore == null) return List.of();
        return bookstore.getInventoryEntries();
    }

}
