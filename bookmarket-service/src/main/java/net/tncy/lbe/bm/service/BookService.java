package net.tncy.lbe.bm.service;

import net.tncy.lbe.bm.data.Book;

import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class BookService {

    private int currentId = 0;
    private final List<Book> books = new ArrayList<>();
    private final Validator validator;

    public BookService(Validator validator) {
        this.validator = validator;
    }

    public int createBook(String title, String publisher, String isbn) throws IllegalArgumentException {
        var id = currentId;
        var book = new Book(id);
        book.setTitle(title);
        book.setPublisher(publisher);
        book.setIsbn(isbn);
        var violations = validator.validate(book);
        if (violations.isEmpty()) {
            currentId++;
            books.add(book);
            return id;
        } else {
            throw new IllegalArgumentException("This book is not valid");
        }
    }

    public void deleteBook(int bookId) {
        books.removeIf(book -> book.getId() == bookId);
    }

    public Book getBookById(int bookId) {
        return books.stream()
                    .filter(book -> book.getId() == bookId)
                    .findFirst()
                    .orElse(null);
    }

    public List<Book> findBookByTitle(String title) {
        return books.stream()
                    .filter(book -> book.getTitle().equals(title))
                    .collect(toList());
    }

    public List<Book> findBookByAuthor(String author) {
        return books.stream()
                    .filter(book -> book.getAuthor().equals(author))
                    .collect(toList());
    }

    public Book getBookByIsbn(String isbn) {
        return books.stream()
                    .filter(book -> book.getIsbn().equals(isbn))
                    .findFirst()
                    .orElse(null);
    }
}
