package net.tncy.lbe.bm;

import net.tncy.lbe.bm.data.Book;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookTest {

    private static Validator validator;

    @BeforeClass
    public static void beforeAll() {
        var factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validIsbn() {
        // Given
        var book = new Book(1);
        book.setIsbn("90-70002-34-5");
        // When
        var violations = validator.validate(book);
        // Then
        assertTrue(violations.isEmpty());
    }

    @Test
    public void illegalCharacters() {
        // Given
        var book = new Book(1);
        book.setIsbn("90/70002/34/5");
        // When
        var violations = validator.validate(book);
        // Then
        assertFalse(violations.isEmpty());
    }
}
