package net.tncy.lbe.bm.data;

import java.util.Objects;

public class InventoryEntry {

    private Book book;
    private int quantity;
    private float averagePrice;
    private float currentPrice;

    public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice) {
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.currentPrice = currentPrice;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public float getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryEntry that = (InventoryEntry) o;
        return quantity == that.quantity && Float.compare(that.averagePrice, averagePrice) == 0 && Float.compare(that.currentPrice, currentPrice) == 0 && Objects.equals(book, that.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(book, quantity, averagePrice, currentPrice);
    }

    @Override
    public String toString() {
        return "InventoryEntry{" +
                "book=" + book +
                ", quantity=" + quantity +
                ", averagePrice=" + averagePrice +
                ", currentPrice=" + currentPrice +
                '}';
    }
}
